<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon">
            <img src="{{ url('images/icon.svg') }}" width="40px" alt="">
        </div>
        <div class="sidebar-brand-text mx-3">
            <img src="{{ url('images/logo.svg') }}" width="120px" alt="">
        </div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href=>
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Travel
    </div>

    <li class="nav-item active">
        <a class="nav-link" href=>
            <i class="fas fa-fw fa-table"></i>
            <span>Paket Travel</span></a>
    </li>

    <li class="nav-item active">
        <a class="nav-link" href=>
            <i class="fas fa-fw fa-image"></i>
            <span>Gallery Travel</span></a>
    </li>

    <li class="nav-item active">
        <a class="nav-link" href=>
            <i class="fas fa-fw fa-dollar-sign"></i>
            <span>Transaksi</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->
