<footer class="section-footer mt-5 border-top">
    <div class="container pt-5 pb-5">
      <div class="row justify-content-center">
        <div class="col-12">
          <div class="row">
            <div class="col-12 col-lg-3">
              <h5>FEATURES</h5>
              <ul class="list-unstyled">
                <li><a href="#">Reviews</a></li>
                <li><a href="">Community</a></li>
                <li><a href="">Social Media Kit</a></li>
                <li><a href="">Affilate</a></li>
              </ul>
            </div>
            <div class="col-12 col-lg-3">
              <h5>ACCOUNT</h5>
              <ul class="list-unstyled">
                <li><a href="#">Reviews</a></li>
                <li><a href="">Community</a></li>
                <li><a href="">Social Media Kit</a></li>
                <li><a href="">Affilate</a></li>
              </ul>
            </div>
            <div class="col-12 col-lg-3">
              <h5>COMPANY</h5>
              <ul class="list-unstyled">
                <li><a href="#">Reviews</a></li>
                <li><a href="">Community</a></li>
                <li><a href="">Social Media Kit</a></li>
                <li><a href="">Affilate</a></li>
              </ul>
            </div>
            <div class="col-12 col-lg-3">
              <h5>GET CONNECTED</h5>
              <ul class="list-unstyled">
                <li><i class="fas fa-building"></i> Bandung Barat</li>
                <li><i class="fas fa-map-marker-alt"></i> Indonesia</li>
                <li><i class="fas fa-phone-alt"></i> 081-1258-1245</li>
                <li><i class="fas fa-envelope"></i> support@nomads.com</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="d-flex justify-content-center p-4 border-top">
        <div class="col-auto text-gray-500 font-weight-light">
          NOMADS - copyright 2021
        </div>
      </div>
    </div>
  </footer>
