@extends('layouts.app')

@section('title','Checkout Travel')

@section('content')
<!-- main -->
<main>
    <section class="section-detail-header">
      <div class="layer"></div>
    </section>

    <section class="section-detail-content">
      <div class="container">
        <div class="row">
          <div class="col p-0">
            <nav>
              <ol class="breadcrumb">
                <li class="breadcrumb-item">Paket Travel</li>
                <li class="breadcrumb-item active">Details</li>
              </ol>
            </nav>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-8 pl-lg-0">
            <div class="card card-details">
              <h4>WHO'S GOING?</h4>
              <p>Trip to Ubud, Bali</p>
              <table class="table align-baseline">
                <thead>
                  <tr>
                    <th>Picture</th>
                    <th>Name</th>
                    <th>Nationality</th>
                    <th>VISA</th>
                    <th>Passport</th>
                    <th>#</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td scope="row">
                      <img
                        src="frondend/images/user.png"
                        width="40px"
                        class="img-table"
                      />
                    </td>
                    <td>Sandi Maulana</td>
                    <td>Indonesia</td>
                    <td>30 Days</td>
                    <td>Active</td>
                    <td>
                      <a href="#"><i class="far fa-times-circle"></i></a>
                    </td>
                  </tr>
                  <tr>
                    <td scope="row">
                      <img
                        src="frondend/images/user.png"
                        width="40px"
                        class="img-table"
                      />
                    </td>
                    <td>Sandi Maulana</td>
                    <td>Indonesia</td>
                    <td>30 Days</td>
                    <td>Active</td>
                    <td>
                      <a href="#"><i class="far fa-times-circle"></i></a>
                    </td>
                  </tr>
                </tbody>
              </table>
              <div class="member mt-5">
                <h5>ADD MEMBER</h5>
                <form action="" class="form-inline">
                  <div class="row">
                    <div class="col">
                      <label for="inputUsername" class="sr-only">nama</label>
                      <input
                        type="text"
                        class="form-control"
                        placeholder="First name"
                        aria-label="First name"
                      />
                    </div>
                    <div class="col">
                      <label for="inputVisa" class="sr-only">nama</label>
                      <select
                        name="inputVisa"
                        id="inputVisa"
                        class="form-control custom-select mb-2 mr-sm-2"
                      >
                        <option value="VISA" selected>VISA</option>
                        <option value="">30 days</option>
                      </select>
                    </div>
                    <div class="col">
                      <div class="form-group mb-2 mr-sm-2">
                        <label for="inputUsername" class="sr-only">tanggal</label>
                        <input
                          type="date"
                          class="form-control"
                        />
                      </div>
                    </div>
                    <div class="col">
                      <button type="submit" class="btn btn-warning">ADD NOW</button>
                    </div>
                  </div>
                </form>
                <hr>
                <div class="disclaimer">
                  <h3 class="mt-2 mb-0">note</h3>
                <p class="mb-0">you are only able to invite member that has registered in nomads</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="card card-right">
              <h5>Checkout Information</h5>
              <table class="trip-information">
                <tr>
                  <td width="50%">Members</td>
                  <td width="50%" class="text-end">22 jan 2012</td>
                </tr>

                <tr>
                  <td width="50%">Additional Visa</td>
                  <td width="50%" class="text-end">22 jan 2012</td>
                </tr>
                <tr>
                  <td width="50%">Trip Price</td>
                  <td width="50%" class="text-end">22 jan 2012</td>
                </tr>
                <tr>
                  <td width="50%">Total Price</td>
                  <td width="50%" class="text-end">22 jan 2012</td>
                </tr>
              </table>
              <hr />
              <h5>Payment Instruction</h5>
              <p class=" text-black-50">
                Please complete the payments before you continue the trip
              </p>
              <div class="bank mt-4">
                <div class="pb-4 float-start">
                  <img src="frondend/images/details/PAYMENT.png" alt="" srcset="">
                </div>
                <div class="ps-4 pb-4 float-start bank-item">
                  <p>PT. NOMADS <br>
                    Bank Central Asia <br>
                    0845 7815 7845
                    </p>
                </div>
              </div>
              <div class="bank">
                <div class="pb-4 float-start">
                  <img src="frondend/images/details/PAYMENT.png" alt="" srcset="">
                </div>
                <div class="ps-4 pb-4 float-start bank-item">
                  <p>PT. NOMADS <br>
                    Bank Central Asia <br>
                    0845 7815 7845
                    </p>
                </div>
              </div>
              <a href="{{ route('success') }}" class="btn btn-join mt-lg-5 px-lg-5">I HAVE PAID</a>
            </div>
          </div>
        </div>
      </div>
    </section>
</main>
@endsection


@push('prepend-style')
<link rel="stylesheet" href="{{ url('frondend/libraries/gijgo/css/gijgo.css') }}">
@endpush


@push('addon-script')
<script src="{{ url('frondend/libraries/gijgo/js/gijgo.min.js') }}"></script>
<script>
    $('.datepicker').datepicker({
      uilibrary: 'bootstrap4',
      icons: {
        rightIcon: '<img src="{{ url('frondend/images/details/ic_outline-date-range.png') }}" >'
      }
    })
</script>
@endpush
