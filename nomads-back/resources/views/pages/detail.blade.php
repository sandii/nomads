@extends('layouts.app')

@section('title','Detail Travel')

@section('content')
<!-- main -->
<main>
    <section class="section-detail-header">
        <div class="layer">
        </div>
    </section>

    <section class="section-detail-content">
        <div class="container">
            <div class="row">
                <div class="col p-0">
                    <nav>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">Paket Travel</li>
                            <li class="breadcrumb-item active">Details</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 pl-lg-0">
                    <div class="card card-details">
                        <h4>UBUD, BALI</h4>
                        <p>INDONESIA</p>
                        <div class="gallery">
                            <div class="xzoom-container">
                                <img src="frondend/images/details/bromo.png" class="xzoom" id="xzoom-default" xoriginal="frondend/images/details/bromo.png"/>
                            </div>
                            <div class="xzoom-thumbs">
                                <a href="frondend/images/details/rajaampat.jpg">
                                <img src="frondend/images/details/rajaampat.jpg" class="xzoom-gallery" xpreview="frondend/images/details/rajaampat.jpg" ></a>
                            </div>
                            <div class="xzoom-thumbs">
                                <a href="frondend/images/details/bromo.png">
                                <img src="frondend/images/details/bromo.png" class="xzoom-gallery" xpreview="frondend/images/details/bromo.png" ></a>
                            </div>
                            <div class="xzoom-thumbs">
                                <a href="frondend/images/details/bromo.png">
                                <img src="frondend/images/details/bromo.png" class="xzoom-gallery" xpreview="frondend/images/details/bromo.png" ></a>
                            </div>
                            <div class="xzoom-thumbs">
                                <a href="frondend/images/details/bromo.png">
                                <img src="frondend/images/details/bromo.png" class="xzoom-gallery"  xpreview="frondend/images/details/bromo.png" ></a>
                            </div>
                        </div>
                        <h6>Tentang Ubud</h6>
                        <p>Daya tarik monkey forest Ubud sebagai tempat liburan, terletak pada hutan lindung yang masih asri dan didalam hutan banyak terdapat kera. Di dalam area hutan juga terdapat pura Hindu yang diberi nama Pura Dalem Agung Padangtegal Ubud.
                        </p>
                        <p>dan didalam hutan banyak terdapat kera. Di dalam area hutan juga terdapat pura Hindu yang diberi nama Pura Dalem Agung Padangtegal Ubud.</p>
                        <div class="features row">
                            <div class="col-md-4 border-right">
                                <div class="description">
                                    <img src="frondend/images/tiket.png" alt="" class="features-img">
                                    <div class="description">
                                        <h6>FEATURED EVENT</h6>
                                    <p>Tari Kecak</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 border-right">
                                <div class="description">
                                    <img src="frondend/images/spech.png" alt="" class="features-img">
                                    <div class="description">
                                        <h6>LANGUAGE</h6>
                                    <p>Indonesia</p>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="description">
                                    <img src="frondend/images/food.png" alt="" class="features-img">
                                    <div class="description">
                                        <h6>FOODS</h6>
                                        <p>lokal food</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card card-right">
                        <h4>ALL MEMBERS</h4>
                        <div class="members my-2">
                            <img src="frondend/images/user2.png" class="member-img">
                            <img src="frondend/images/user2.png" class="member-img">
                            <img src="frondend/images/user2.png" class="member-img">
                            <img src="frondend/images/user2.png" class="member-img">
                        </div>
                        <hr>
                        <h5>trip information</h5>
                        <table class="trip-information">
                            <tr>
                                <th width="50%">date of departure</th>
                                <td width="50%" class="text-end">
                                    22 jan 2012
                                </td>
                            </tr>

                            <tr>
                                <th width="50%">date of departure</th>
                                <td width="50%" class="text-end">
                                    22 jan 2012
                                </td>
                            </tr>
                            <tr>
                                <th width="50%">date of departure</th>
                                <td width="50%" class="text-end">
                                    22 jan 2012
                                </td>
                            </tr>
                            <tr>
                                <th width="50%">date of departure</th>
                                <td width="50%" class="text-end">
                                    22 jan 2012
                                </td>
                            </tr>
                        </table>
                        <a href="{{ route('checkout') }}" class="btn btn-join mt-lg-5 px-lg-5">JOIN NOW</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>
@endsection

@push('prepend-style')
<link rel="stylesheet" href="{{ url('frondend/libraries/xzoom/xzoom.css') }}">
@endpush


@push('addon-script')
<script src="{{ url('frondend/libraries/xzoom/xzoom.min.js') }}"></script>
    <script>
     $(document).ready(function () {
         $('.xzoom, .xzoom-gallery').xzoom({
             zoomWidth : 500,
             title: false,
             tint: '#333',
             xoffset: 15,
         });
     })
    </script>
@endpush
