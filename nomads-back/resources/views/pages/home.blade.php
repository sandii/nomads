@extends('layouts.app')

@section('title','NOMADS')


@section('content')
<!-- header -->
<header class="text-center">
    <h1>
      Explore The Worlds, <br />
      As Easy on Click
    </h1>
    <p class="mt-3">
      You will see beautiful moment you <br />
      can't beyond your imagination
    </p>
    <a href="" class="btn btn-get-started px-4 mt-4"> get started </a>
  </header>

  <!-- main -->
  <main>
    <section class="section-background">
    </section>

    <section
      class="section-stats row justify-content-center text-center"
      id="stats"
    >
      <div class="col-3 col-md-2 stats-detail">
        <h3>20K</h3>
        <p>Member</p>
      </div>
      <div class="col-3 col-md-2 stats-detail">
        <h3>12</h3>
        <p>Countries</p>
      </div>
      <div class="col-3 col-md-2 stats-detail">
        <h3>3K</h3>
        <p>Hotels</p>
      </div>
      <div class="col-3 col-md-2 stats-detail">
        <h3>72</h3>
        <p>Partners</p>
      </div>
    </section>

    <section class="section-popular" id="popular">
      <div class="container">
        <div class="row">
          <div class="col text-center popular-heading">
            <hr />
            <h2>Popular Travels</h2>
            <p>
              something that you never try <br />
              before in this worlds
            </p>
          </div>
        </div>
      </div>
    </section>

    <section class="section-content" id="section-content">
      <div class="container">
        <div class="section-travel row justify-content-center">
          <div class="col-sm-6 col-md-4 col-lg-3">
            <div class="card-travel text-center d-flex flex-column" style="background-image: url('frondend/images/bromo.jpg');">
              <div class="travel-kota">ubud, bali</div>
              <div class="travel-country">indonesia</div>
              <div class="travel-button mt-auto">
                <a href="{{ route('detail') }}" class="btn btn-travel px-4">view details</a>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-4 col-lg-3">
            <div class="card-travel text-center d-flex flex-column" style="background-image: url('frondend/images/rajaampat.jpg');">
              <div class="travel-kota">ubud, bali</div>
              <div class="travel-country">indonesia</div>
              <div class="travel-button mt-auto">
                <a href="#" class="btn btn-travel px-4">view details</a>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-4 col-lg-3">
            <div class="card-travel text-center d-flex flex-column" style="background-image: url('frondend/images/ubud.jpg');">
              <div class="travel-kota">ubud, bali</div>
              <div class="travel-country">indonesia</div>
              <div class="travel-button mt-auto">
                <a href="#" class="btn btn-travel px-4">view details</a>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-4 col-lg-3">
            <div class="card-travel text-center d-flex flex-column" style="background-image: url('frondend/images/bromo.jpg');">
              <div class="travel-kota">ubud, bali</div>
              <div class="travel-country">indonesia</div>
              <div class="travel-button mt-auto">
                <a href="#" class="btn btn-travel px-4">view details</a>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-4 col-lg-3">
            <div class="card-travel text-center d-flex flex-column" style="background-image: url('frondend/images/rajaampat.jpg');">
              <div class="travel-kota">ubud, bali</div>
              <div class="travel-country">indonesia</div>
              <div class="travel-button mt-auto">
                <a href="#" class="btn btn-travel px-4">view details</a>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-4 col-lg-3">
            <div class="card-travel text-center d-flex flex-column" style="background-image: url('frondend/images/ubud.jpg');">
              <div class="travel-kota">ubud, bali</div>
              <div class="travel-country">indonesia</div>
              <div class="travel-button mt-auto">
                <a href="#" class="btn btn-travel px-4">view details</a>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-4 col-lg-3">
            <div class="card-travel text-center d-flex flex-column" style="background-image: url('frondend/images/bromo.jpg');">
              <div class="travel-kota">ubud, bali</div>
              <div class="travel-country">indonesia</div>
              <div class="travel-button mt-auto">
                <a href="#" class="btn btn-travel px-4">view details</a>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-4 col-lg-3">
            <div class="card-travel text-center d-flex flex-column" style="background-image: url('frondend/images/rajaampat.jpg');">
              <div class="travel-kota">ubud, bali</div>
              <div class="travel-country">indonesia</div>
              <div class="travel-button mt-auto">
                <a href="#" class="btn btn-travel px-4">view details</a>
              </div>
            </div>
          </div>

        </div>
      </div>
    </section>

    <section class="section-whyus">
      <div class="container">
        <div class="row">
          <div class="col text-center popular-heading">
            <hr />
            <h2>Why With Us</h2>
            <p>
              something that you never try <br />
              before in this worlds
            </p>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6 col-md-4 col-lg-3">
            <div class="card-whyus text-center d-flex flex-column">
              <div class="icon-whyus text-center">
                <i class="fas fa-medal"></i>
              </div>
              <h3>great service</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Non libero vulputate et ac cursus dolor </p>
            </div>
          </div>
          <div class="col-sm-6 col-md-4 col-lg-3">
            <div class="card-whyus text-center d-flex flex-column">
              <div class="icon-whyus text-center">
                <i class="fas fa-money-bill-wave"></i>
              </div>
              <h3>best price</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Non libero vulputate et ac cursus dolor </p>
            </div>
          </div>
          <div class="col-sm-6 col-md-4 col-lg-3">
            <div class="card-whyus text-center d-flex flex-column">
              <div class="icon-whyus text-center">
                <i class="fas fa-user-tie"></i>
              </div>
              <h3>professional</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Non libero vulputate et ac cursus dolor </p>
            </div>
          </div>
          <div class="col-sm-6 col-md-4 col-lg-3">
            <div class="card-whyus text-center d-flex flex-column">
              <div class="icon-whyus text-center">
                <i class="fas fa-heart"></i>
              </div>
              <h3>best value</h3>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Non libero vulputate et ac cursus dolor </p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="bg-love">
    </section>

    <section class="section-love">
      <div class="container">
        <div class="row">
          <div class="col-md-4 text-love">
            <hr />
            <h2>They Love With Us</h2>
            <p>You will see beautiful moment you You will see beautiful moment youYou will see beautiful moment you
            </p>
          </div>
          <div class="col-sm-6 col-lg-p">
            <div class="card-love text-center d-flex flex-column">
              <img src="frondend/images/user.png" alt="">
              <h4>Sandi Maulana</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Non libero vulputate et ac cursus dolor </p>
            </div>
          </div>
          <div class="col-sm-6 col-lg-p">
            <div class="card-love text-center d-flex flex-column">
              <img src="frondend/images/user2.png" alt="">
              <h4>Sandi Maulana</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Non libero vulputate et ac cursus dolor </p>
            </div>
          </div>
          <div class="col-sm-6 col-lg-p">
            <div class="card-love text-center d-flex flex-column">
              <img src="frondend/images/user.png" alt="">
              <h4>Sandi Maulana</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Non libero vulputate et ac cursus dolor </p>
            </div>
          </div>
          <hr class="garis">
        </div>
      </div>
    </section>

    <section class="section-network">
        <div class="container">
          <div class="row">
            <div class="col-lg-4 ">
              <hr>
              <h2>Network</h2>
              <p>You will see beautiful moment you You will see beautiful moment youYou will see beautiful moment you
              </p>
            </div>
            <div class="col-lg-2 col-test">
              <div class="card-network text-center d-flex flex-column">
                <img src="frondend/images/logo/apple.png" alt="">
              </div>
            </div>
            <div class="col-lg-2 col-test">
              <div class="card-network text-center d-flex flex-column">
                <img src="frondend/images/logo/fedex.png" alt="">
              </div>
            </div>
            <div class="col-lg-2 col-test">
              <div class="card-network text-center d-flex flex-column">
                <img src="frondend/images/logo/xiaomi.png" alt="">
              </div>
            </div>
            <div class="col-lg-2 col-test">
              <div class="card-network text-center d-flex flex-column">
                <img src="frondend/images/logo/lg.png" alt="">
              </div>
            </div>
          </div>
        </div>
      </section>
  </main>
@endsection
