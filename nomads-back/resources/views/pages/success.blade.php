@extends('layouts.success')

@section('content')
 <!-- main -->
 <main>
    <div class="row">
      <div class="col-lg-8">
        <section class="top">
          <div class="container-fluid">
            <div class="row mt-lg-5 ms-lg-5">
              <div class="col-lg-4 text-center">
                <a class="navbar-brand" href="{{ route('home') }}">
                  <img src="frondend/images/logo.png" alt="logo nomads" />
                </a>
              </div>
              <div class="col text-logo">
                <h3>
                  EXPLORE THE WORLDS, <br />
                  AS EASY ONE CLICK
                </h3>
              </div>
            </div>
          </div>
        </section>
        <section class="content-center text-center">
          <img src="frondend/images/sukses.png" alt="" />
          <div class="container mt-lg-5 mb-lg-5 text-center">
            <h2>YAY SUCCESS!!!</h2>
            <h5>
              we'have sent you email for trip instruction <br> please read it is
              well
            </h5>
            <a href="index.html" class="btn btn-get-started px-4 mt-5 mb-lg-5"> back to home </a>
          </div>
          <p></p>
        </section>
      </div>
      <div class="col-4">
        <div class="hero"></div>
      </div>
    </div>
  </main>
@endsection
